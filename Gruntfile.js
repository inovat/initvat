'use strict';

module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        meta: {
            baseRoot: 'www',
            basePath: 'www/assets',
            deployRoot: 'dist',
            deployPath: 'dist/assets'
        },
        clean: ['www/tmp'],
        coffee: {
            dev: {
                files: {
                    'www/tmp/js/coffee.js': []
                }
            }
        },
        compass: {
            options: {
                sassDir: '<%= meta.basePath %>/scss',
                cssDir: '<%= meta.basePath %>/styles'
            },
            dev: {
                options: {
                    cssDir: '<%= meta.basePath %>/styles',
                    outputStyle: 'expanded'
                }
            },
            dist: {
                options: {
                    cssDir: '<%= meta.deployPath %>/styles',
                    outputStyle: 'compressed'
                }
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            dev: {
                src: [
                    '<%= meta.basePath %>/components/jquery/jquery.js',
                    'www/tmp/js/coffee.js',
                    '<%= meta.basePath %>/js/main.js'
                ],
                dest: 'www/tmp/js/<%= pkg.name %>.js'
            }
        },
        imagemin: {
            dist: {
                options: {
                    cache: false
                },
                files: [{
                    expand: true,
                    cache: false,
                    cwd: '<%= meta.basePath %>/images',
                    src: '**/*.{gif,jpeg,jpg,png}',
                    dest: '<%= meta.deployPath %>/images'
                }]
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    '<%= meta.deployPath %>/js/<%= pkg.name %>.min.js': ['<%= concat.dev.dest %>']
                }
            }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                white: false,
                shadow: true,
                eqeqeq: false
            },
            files: [
                'Gruntfile.js',
                '<%= meta.basePath %>/js/**/*.js'
            ]
        },
        copy: {
            dist: {
                files: [
                    { expand: true, cwd: '<%= meta.basePath %>/fonts/', src: '**', dest: '<%= meta.deployPath %>/fonts/'},
                    { expand: true, cwd: '<%= meta.baseRoot %>/', src: '*.html', dest: '<%= meta.deployRoot %>/'},
                    { expand: true, cwd: '<%= meta.baseRoot %>/views/', src: '**/*.html', dest: '<%= meta.deployRoot %>/views/'}
                ]
            }
        },
        watch: {
            js: {
                files: ['<%= meta.basePath %>/js/**/*.coffee'],
                tasks: ['clean', 'jshint', 'coffee', 'concat']
            },
            css: {
                files: ['<%= meta.basePath %>/scss/**/*.scss'],
                tasks: ['compass:dev']
            }
        }
    });

    // Load all grunt tasks
    require('load-grunt-tasks')(grunt);

    grunt.registerTask('default', [
        'clean',
        'jshint',
        'compass',
        'coffee',
        'concat',
        'imagemin',
        'uglify',
        'copy'
    ]);

};